using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartUp : GameState {

    public StartUp(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

    public override void Start() {
        Debug.Log("State: StartUp");
        mainCamerTransform = Camera.main.transform;
        Vector3 firstPosition = mainCamerTransform.position;
        endPosition = firstPosition + new Vector3(0.0f, moveDistance, 0.0f);

        GameObject steak = ControllableObjects.Instance.Container["Steak"];
        steak.SetActive(true);
        steak.transform.position = Vector3.up * (moveDistance - 0.5f);

        GameObject meshCutterObject = ControllableObjects.Instance.Container["MeshCutter"];
        meshCutterObject.SetActive(true);
        Vector3 meshCutterPosition = steak.transform.position;
        meshCutterPosition.x = meshCutterObject.transform.position.x;
        meshCutterPosition.y += 2.0f; 
        meshCutterPosition.z = meshCutterObject.transform.position.z;
        meshCutterObject.transform.position = meshCutterPosition;

        Vector3 meshCutterMoveStartPosition = meshCutterObject.transform.position + Vector3.up * 0f;
        Vector3 meshCutterMoveEndPosition = meshCutterMoveStartPosition - Vector3.up * 2.0f;
        meshCutter = meshCutterObject.GetComponent<ExampleUseof_MeshCut>();
        meshCutter.InithalizeMoveParameter(meshCutterMoveStartPosition, meshCutterMoveEndPosition);
    }

    public override void Update() {
        if (IsEndState) {
            return;
        }

        mainCamerTransform.position = Vector3.Lerp(mainCamerTransform.position, endPosition, moveSpeed * Time.deltaTime);
        Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, backgroundColor, moveSpeed * Time.deltaTime);

        meshCutter.MoveMeshCutter();

        float distance = Vector3.Distance(mainCamerTransform.position, endPosition);
        if(distance <= epsilon && !IsEndState) {
            IsEndState = true;
        }
    }

    public override void End() {
        mainCamerTransform.position = endPosition;
        Camera.main.backgroundColor = backgroundColor;

        GameObject steakStandard = ControllableObjects.Instance.Container["SteakStandard"];
        steakStandard.SetActive(false);
    }

    public override GameState MoveNextState() {
        return new Cutting(monoBehaviour);
    }

    Transform mainCamerTransform;
    readonly float moveDistance = 8.0f;
    readonly float moveSpeed = 8.0f;
    readonly float epsilon = 0.05f;
    Vector3 endPosition;

    Color backgroundColor = Color.cyan;

    ExampleUseof_MeshCut meshCutter;
}
