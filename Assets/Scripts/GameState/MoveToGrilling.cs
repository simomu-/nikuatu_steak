using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToGrilling : GameState {

    public MoveToGrilling(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

    public override void Start() {
        Debug.Log("State: MoveToGrilling");
        mainCamerTransform = Camera.main.transform;
        Vector3 firstPosition = mainCamerTransform.position;
        endPosition = firstPosition - new Vector3(0.0f, moveDistance, 0.0f);

        GameObject burnersObject = ControllableObjects.Instance.Container["Burners"];
        burnersObject.SetActive(true);
        Burner burner = burnersObject.GetComponent<Burner>();
        burner.StartBurner();

        GameObject steakObject = ControllableObjects.Instance.Container["Steak"];
        steakTransform = steakObject.transform;
        Steak steak = steakObject.GetComponent<Steak>();
        steakEndPosition = Vector3.up * (moveDistance - 0.5f) - Vector3.up * (steak.Thickness * 2.0f);
    }

    public override void Update() {
        if (IsEndState) {
            return;
        }

        mainCamerTransform.position = Vector3.Lerp(mainCamerTransform.position, endPosition, moveSpeed * Time.deltaTime);
        Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, backgroundColor, moveSpeed * Time.deltaTime);

        steakTransform.position = Vector3.Lerp(steakTransform.position, steakEndPosition, moveSpeed * Time.deltaTime);

        float distance = Vector3.Distance(mainCamerTransform.position, endPosition);
        if (distance <= epsilon && !IsEndState) {
            IsEndState = true;
        }
    }

    public override void End() {
        mainCamerTransform.position = endPosition;
        Camera.main.backgroundColor = backgroundColor;

        steakTransform.position = steakEndPosition;
    }

    public override GameState MoveNextState() {
        return new Grilling(monoBehaviour);
    }

    Transform mainCamerTransform;
    readonly float moveDistance = 4.0f;
    readonly float moveSpeed = 10.0f;
    readonly float epsilon = 0.01f;
    Vector3 endPosition;

    readonly Color backgroundColor = new Color(1.0f, 126f / 255f, 0.0f);

    Transform steakTransform;
    Vector3 steakEndPosition;

}
