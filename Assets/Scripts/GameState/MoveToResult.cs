using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToResult : GameState {

    public MoveToResult(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

    public override void Start() {
        Debug.Log("State: MoveToResult");
        mainCamerTransform = Camera.main.transform;
        Vector3 firstPosition = mainCamerTransform.position;
        endPosition = firstPosition - new Vector3(0.0f, moveDistance, 0.0f);

        GameObject steakObject = ControllableObjects.Instance.Container["Steak"];
        Steak steak = steakObject.GetComponent<Steak>();
        steakTransform = steakObject.transform;
        Debug.LogFormat("Thickness: {0}", steak.Thickness);
        steakEndPosition = Vector3.zero - Vector3.up * (steak.Thickness * 2.0f);

        GameObject seObject = ControllableObjects.Instance.Container["SoundEffectPlayer"];
        SoundEffectPlayer soundEffectPlayer = seObject.GetComponent<SoundEffectPlayer>();
        soundEffectPlayer.StopBurnerAudio();
    }

    public override void Update() {
        if (IsEndState) {
            return;
        }

        mainCamerTransform.position = Vector3.Lerp(mainCamerTransform.position, endPosition, moveSpeed * Time.deltaTime);
        Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, backgroundColor, moveSpeed * Time.deltaTime);

        steakTransform.position = Vector3.Lerp(steakTransform.position, steakEndPosition, moveSpeed * Time.deltaTime);

        float distance = Vector3.Distance(mainCamerTransform.position, endPosition);
        if (distance <= epsilon && !IsEndState) {
            IsEndState = true;
        }
    }

    public override void End() {
        mainCamerTransform.position = endPosition;
        Camera.main.backgroundColor = backgroundColor;

        steakTransform.position = steakEndPosition;
    }

    public override GameState MoveNextState() {
        return new Result(monoBehaviour);
    }

    Transform mainCamerTransform;
    readonly float moveDistance = 4.0f;
    readonly float moveSpeed = 10.0f;
    readonly float epsilon = 0.01f;
    Vector3 endPosition;

    readonly Color backgroundColor = Color.white;

    Transform steakTransform;
    Vector3 steakEndPosition;
}
