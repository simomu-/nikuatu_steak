using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GameState {

    public GameState(MonoBehaviour monoBehaviour) {
        this.monoBehaviour = monoBehaviour;
    }

    public abstract void Start();

    public abstract void Update();

    public abstract void End();

    public abstract GameState MoveNextState();

    public UnityAction OnUserInput;

    public bool IsEndState = false;

    protected MonoBehaviour monoBehaviour;

}
