using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Result : GameState {

    public Result(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

    public override void Start() {
        Debug.Log("State: Result");

        GameObject burnersObject = ControllableObjects.Instance.Container["Burners"];
        burnersObject.SetActive(false);

        GameObject steakObject = ControllableObjects.Instance.Container["Steak"];
        steak = steakObject.GetComponent<Steak>();

        Debug.LogFormat("Thickness: {0}", 1.0f - steak.Thickness);
        Debug.LogFormat("GrillLevel: {0}", steak.GrillLevel);

        float diffToCritical = Mathf.Abs(steak.GrillLevel - 0.5f);
        grillPoint = 0.5f - diffToCritical;

        thickness = (1.0f - steak.Thickness) / 2.0f;
        result = (thickness + grillPoint) * 100.0f;
        Debug.LogFormat("Thickness point: {0}", thickness);
        Debug.LogFormat("Grill point: {0}", grillPoint);
        Debug.LogFormat("Result: {0}", result);

        GameObject resultPanelObject = ControllableObjects.Instance.Container["ResultPanel"];
        resultPanel = resultPanelObject.GetComponent<ResultPanel>();

        monoBehaviour.StartCoroutine(DisplayResultCoroutine());

        Dictionary<string, object> resultParam = new Dictionary<string, object>();
        resultParam["ThicknessPoint"] = thickness * 100.0f;
        resultParam["GrillPoint"] = grillPoint * 100.0f;
        resultParam["GrillLevel"] = steak.GrillLevel * 100.0f;
        resultParam["Result"] = result;
        Analytics.CustomEvent("Result", resultParam);
    }

    IEnumerator DisplayResultCoroutine() {
        yield return new WaitForSeconds(displayResultWaitTime);
        isMoveCameraForResult = true;
        resultPanel.SetUpResultPanel(thickness, grillPoint, result, steak.GrillLevel);
    }

    public override void Update() {
        if (!isMoveCameraForResult) {
            return;
        }

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, resultCameraPosition, moveSpeed * Time.deltaTime);
        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, resultCameraSize, moveSpeed * Time.deltaTime);
    }

    public override void End() {
        
    }

    public override GameState MoveNextState() {
        return null;
    }

    Steak steak;
    bool isMoveCameraForResult = false;
    readonly Vector3 resultCameraPosition = new Vector3(3.82f, 3.14f, -2.95f);
    readonly float resultCameraSize = 2.0f;
    readonly float moveSpeed = 5.0f;
    readonly float displayResultWaitTime = 0.75f;

    float thickness;
    float grillPoint;
    float result;

    ResultPanel resultPanel;
}
