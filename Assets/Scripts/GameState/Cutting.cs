using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutting : GameState {

    public Cutting(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

    public override void Start() {
        Debug.Log("State: Cutting");

        GameObject cutter = ControllableObjects.Instance.Container["MeshCutter"];
        meshCutter = cutter.GetComponent<ExampleUseof_MeshCut>();
        meshCutterMoveStartPosition = meshCutter.StartPosition + Vector3.up * 0f;
        meshCutterMoveEndPosition = meshCutterMoveStartPosition - Vector3.up * 2.0f;

        OnUserInput = OnCutInput;
    }

    public override void Update() {
        meshCutter.MoveMeshCutter();
    }

    public override void End() {
        
    }

    public override GameState MoveNextState() {
        return new MoveToGrilling(monoBehaviour);
    }

    public void OnCutInput() {
        if (IsEndState) {
            return;
        }

        meshCutter.MeshCut();
        float movableDistance = Vector3.Distance(meshCutterMoveStartPosition, meshCutterMoveEndPosition);
        float currentNormalizedPosition = (meshCutter.transform.position.y - meshCutterMoveEndPosition.y) / movableDistance;

        GameObject steakObject = ControllableObjects.Instance.Container["Steak"];
        Steak steak = steakObject.GetComponent<Steak>();
        steak.Thickness = currentNormalizedPosition;
        Debug.LogFormat("Thicness: {0}", steak.Thickness);

        GameObject seObject = ControllableObjects.Instance.Container["SoundEffectPlayer"];
        SoundEffectPlayer soundEffectPlayer = seObject.GetComponent<SoundEffectPlayer>();
        soundEffectPlayer.PlayCutAudio();

        cutCount++;
        Debug.LogFormat("Cut. count: {0}", cutCount);
        if(cutCount >= maxCutCount) {
            IsEndState = true;
        }
    }

    int cutCount = 0;
    readonly int maxCutCount = 1;
    ExampleUseof_MeshCut meshCutter;
    Vector3 meshCutterMoveStartPosition;
    Vector3 meshCutterMoveEndPosition;
}
