using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grilling : GameState {

    public Grilling(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

    public override void Start() {
        Debug.Log("State: Grilling");
        GameObject burnersObject = ControllableObjects.Instance.Container["Burners"];
        burner = burnersObject.GetComponent<Burner>();

        GameObject steakObject = ControllableObjects.Instance.Container["Steak"];
        steak = steakObject.GetComponent<Steak>();
        steak.SetUpGrill();

        OnUserInput = OnStopFire;

        GameObject seObject = ControllableObjects.Instance.Container["SoundEffectPlayer"];
        SoundEffectPlayer soundEffectPlayer = seObject.GetComponent<SoundEffectPlayer>();
        soundEffectPlayer.PlayBurnerAudio();
    }

    public override void Update() {
        if (IsEndState) {
            return;
        }

        steak.OnGriling();
    }

    public override void End() {
        burner.StopBurner();
    }

    public override GameState MoveNextState() {
        return new MoveToResult(monoBehaviour);
    }

    public void OnStopFire() {
        if (IsEndState) {
            return;
        }
        IsEndState = true;
    }

    Burner burner;
    Steak steak;
}
