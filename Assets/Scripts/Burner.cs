using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burner : MonoBehaviour {

    [SerializeField] List<ParticleSystem> burnerParticles;

    public void StartBurner() {
        Debug.Log("Start burner");
        foreach(var particle in burnerParticles) {
            particle.Play();
        }
    }

    public void StopBurner() {
        foreach(var particle in burnerParticles) {
            particle.Stop();
        }
    }

}
