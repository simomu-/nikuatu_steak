using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateProvider : MonoBehaviour {

    public static GameStateProvider Instance {
        get { return instance; }
    }

    static GameStateProvider instance;

    GameState currentGameState;

	void Start () {
        instance = this;
	}
	
	void Update () {
        if(currentGameState == null) {
            return;
        }

        currentGameState.Update();
        if (currentGameState.IsEndState) {
            MoveToNextState();
        }
	}

    public void OnGameStart() {
        currentGameState = new StartUp(this);
        currentGameState.Start();
    }

    public void MoveToNextState() {
        currentGameState?.End();
        currentGameState = currentGameState.MoveNextState();
        currentGameState?.Start();
    }

    public bool OnUserInput() {
        var onUserInput = currentGameState?.OnUserInput;
        if(onUserInput == null) {
            return false;
        }
        onUserInput();
        return true;
    }
}
