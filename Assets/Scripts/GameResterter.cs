using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameResterter : SingletonMonoBehaviour<GameResterter> {

    void Start() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public void Restart() {
        Debug.Log("Restart");
        restarted = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode) {

        if(loadSceneMode != LoadSceneMode.Single) {
            return;
        }

        if (!restarted) {
            return;
        }

        TitlePanel titlePanel = FindObjectOfType<TitlePanel>();
        titlePanel.SkipTitle();
        Debug.Log("SkipTitle");
    }

    bool restarted = false;

}
