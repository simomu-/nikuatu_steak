using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteakNameCreater {

    static readonly string[] thicknessNameTable = {
        "極薄",
        "普通の",
        "ちょい厚めの",
        "肉厚",
        "極厚",
        "超極厚",
        "常識外の厚さの",
        "タワー型",
        "ほぼ切ってない"
        };

    static readonly string[] grillLevelNameTable = {
        "生肉",
        "一瞬火を通した",
        "半生",
        "レア",
        "ミディアム",
        "ウエルダン",
        "焼き過ぎ",
        "丸こげ",
        "炭化"
    };

    public static string GetThicknessName(float value) {
        int index = Mathf.FloorToInt((value * 0.9f) * 10.0f);
        return thicknessNameTable[index];
    }

    public static string GetGrillLevelName(float value) {
        int index = Mathf.FloorToInt((value * 0.9f) * 10.0f);
        return grillLevelNameTable[index];
    }

}
