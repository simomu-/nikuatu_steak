using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanel : MonoBehaviour {

    [SerializeField]
    Image backgroundImage;
    [SerializeField]
    Text thicknessText;
    [SerializeField]
    Text grillPointText;
    [SerializeField]
    Text resultText;
    [SerializeField]
    Text steakNameText;

    void Start() {
        backgroundImage.gameObject.SetActive(false);

        fadeTexts = backgroundImage.GetComponentsInChildren<Text>(true);
        foreach (Text text in fadeTexts) {
            Color color =text.color;
            color.a = 0.0f;
            text.color = color;
        }

        fadeImages = backgroundImage.GetComponentsInChildren<Image>(true);
        foreach(Image image in fadeImages) {
            Color color = image.color;
            color.a = 0.0f;
            image.color = color;
        }
    }

    public void SetUpResultPanel(float thickness, float grillPoint, float result, float grillRowLevel) {
        this.thickness = thickness;
        this.grillPoint = grillPoint;
        this.result = result;
        this.grillRowLevel = grillRowLevel;
        this.steakName = SteakNameCreater.GetThicknessName(this.thickness * 2.0f) + SteakNameCreater.GetGrillLevelName(this.grillRowLevel) + "ステーキ";

        StartCoroutine(DisplayResultCoroutine());
    }

    public void OnRestartButtonClick() {
        GameResterter.Instance.Restart();
    }

    public void OnTweetButtonClick() {
        string tweetText = string.Format("肉の厚さ:{0:0.00}点\n焼き加減:{1:0.00}点\n総得点:{2:0.00}点\n名称:{3}\n", thickness * 100.0f, grillPoint * 100.0f ,result, steakName);
        naichilab.UnityRoomTweet.Tweet("nikuatu_steak", tweetText, "肉アツステーキ", "unity1week");
    }

    public void OnRankingButtonClick() {
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking(result);
    }

    IEnumerator DisplayResultCoroutine() {
        DisplayResultText();
        backgroundImage.gameObject.SetActive(true);
        float time = 0;
        while(time <= fadeInTime) {
            time += Time.deltaTime;
            foreach (Text text in fadeTexts) {
                Color textColor = text.color;
                textColor.a = Mathf.Lerp(0.0f, 1.0f, time / fadeInTime);
                text.color = textColor;
            }
            foreach(Image image in fadeImages) {
                Color textColor = image.color;
                textColor.a = Mathf.Lerp(0.0f, 1.0f, time / fadeInTime);
                image.color = textColor;
            }

            Color color = backgroundImage.color;
            color.a = Mathf.Lerp(0.0f, 0.5f, time / fadeInTime);
            backgroundImage.color = color;
            yield return null;
        }
    }

    void DisplayResultText() {
        thicknessText.text = string.Format("肉の厚さ: {0:0.00}点", thickness * 100.0f);
        grillPointText.text = string.Format("焼き加減: {0:0.00}点", grillPoint * 100.0f);
        resultText.text = string.Format("総合得点: {0:0.00}点", result);
        steakNameText.text = string.Format("名称: {0}", steakName);
    }

    float thickness;
    float grillPoint;
    float grillRowLevel;
    float result;
    string steakName;

    Text[] fadeTexts;
    Image[] fadeImages;

    readonly float fadeInTime = 0.5f;
}
