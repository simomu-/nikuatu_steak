using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrillPanel : MonoBehaviour {

    [SerializeField]
    UIScroller uiScroller;
    [SerializeField]
    GameStateProvider gameStateProvider;

    void Update() {
        if ((PlayerInput.Instance.IsOnKeyboadInput || PlayerInput.Instance.IsOnMouceClick) && uiScroller.PanelIndex == 1) {
            OnInterruptGrillButtonClick();
        }
    }

    public void OnInterruptGrillButtonClick() {
        if (gameStateProvider.OnUserInput()) {
            uiScroller?.MoveDown();
        }
    }
}
