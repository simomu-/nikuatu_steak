using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitlePanel : MonoBehaviour {

    [SerializeField]
    UIScroller uiScroller;
    [SerializeField]
    GameStateProvider gameStateProvider;

    void Update() {
        if (PlayerInput.Instance.IsOnKeyboadInput && uiScroller.PanelIndex == 3) {
            OnStartButtonClick();
        }
    }

    public void OnStartButtonClick() {
        uiScroller?.MoveDown();
        gameStateProvider?.OnGameStart();
    }

    public void SkipTitle() {
        uiScroller?.SetPanel(2);
        gameStateProvider?.OnGameStart();
    }

}
