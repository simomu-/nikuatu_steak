using UnityEngine;
using UnityEngine.UI;

public class UIScroller : MonoBehaviour {

	[SerializeField]
    ScrollRect scrollRect;
	[SerializeField]
    RectTransform content;
	[SerializeField]
    float moveSpeed = 10f;

	void Awake () {
		elementCountMax = content.childCount - 1;
        elementIndex = elementCountMax;
        scrollRect.verticalNormalizedPosition = 1.0f;
    }

    void Update () {
		float position = (float)elementIndex / elementCountMax;
		scrollRect.verticalNormalizedPosition = Mathf.Lerp (scrollRect.verticalNormalizedPosition, position, moveSpeed * Time.deltaTime);
	}

	public void MoveUp(){
		if (elementIndex < elementCountMax) {
			elementIndex++;
		}
	}

	public void MoveDown(){
        if (elementIndex > 0) {
            elementIndex--;
		}
	}

    public void SetPanel(int index) {
        elementIndex = index;
        float position = (float)elementIndex / elementCountMax;
        scrollRect.verticalNormalizedPosition = position;
    }

    public int PanelIndex {
        get { return elementIndex; }
    }

    int elementIndex = 0;
    int elementCountMax;

}
