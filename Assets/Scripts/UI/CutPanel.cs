using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutPanel : MonoBehaviour {

    [SerializeField]
    UIScroller uiScroller;
    [SerializeField]
    GameStateProvider gameStateProvider;

    void Update() {
        if((PlayerInput.Instance.IsOnKeyboadInput || PlayerInput.Instance.IsOnMouceClick) && uiScroller.PanelIndex == 2) {
            OnCutButtonClick();
        }
    }

    public void OnCutButtonClick() {
        if (gameStateProvider.OnUserInput()) {
            uiScroller?.MoveDown();
        }
    }

}
