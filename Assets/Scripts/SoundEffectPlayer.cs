using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectPlayer : MonoBehaviour {

    [SerializeField]
    AudioClip cutAudioClip;
    [SerializeField]
    AudioClip burnerAudioClip;

    void Awake() {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayCutAudio() {
        audioSource.volume = cutVolume;
        audioSource.loop = false;
        audioSource.PlayOneShot(cutAudioClip);
    }

    public void PlayBurnerAudio() {
        audioSource.volume = burnerVolume;
        audioSource.clip = burnerAudioClip;
        audioSource.loop = true;
        audioSource.Play();
        StartCoroutine(PlayBurnderAudioCoroutine());
    }

    public void StopBurnerAudio() {
        StartCoroutine(StopBurnerAudioCoroutine());
    }

    IEnumerator PlayBurnderAudioCoroutine() {
        float time = 0;
        while(time <= audioFadeOutTime) {
            audioSource.volume = Mathf.Lerp(0.0f, 0.2f, time / audioFadeOutTime);
            time += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator StopBurnerAudioCoroutine() {
        float time = 0;
        while(time <= audioFadeOutTime) {
            audioSource.volume = Mathf.Lerp(0.2f, 0.0f, time / audioFadeOutTime);
            time += Time.deltaTime;
            yield return null;
        }
        audioSource.Stop();
    }

    AudioSource audioSource;
    readonly float audioFadeOutTime = 0.3f;
    readonly float cutVolume = 0.5f;
    readonly float burnerVolume = 0.2f;
}
