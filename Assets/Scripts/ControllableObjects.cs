using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllableObjects : MonoBehaviour {

    public static ControllableObjects Instance {
        get { return instance; }
    }

    private static ControllableObjects instance;

    void Awake() {
        instance = this;

        foreach(var registerdObject in registerdObjects) {
            Container.Add(registerdObject.name, registerdObject);
        }
    }

    [SerializeField] List<GameObject> registerdObjects;

    public Dictionary<string, GameObject> Container = new Dictionary<string, GameObject>();


}
