using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInput : MonoBehaviour {

    public static PlayerInput Instance {
        get; private set;
    }

    public bool IsOnMouceClick;
    public bool IsOnKeyboadInput;

    void Awake() {
        Instance = this;
    }

    void Update() {
        IsOnKeyboadInput = Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space);
        IsOnMouceClick = Input.GetMouseButtonDown(0);
    }


}
