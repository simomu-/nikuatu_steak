using UnityEngine;
using System.Collections;

public class ExampleUseof_MeshCut : MonoBehaviour {

	public Material capMaterial;
    [System.NonSerialized]
    public Vector3 StartPosition;
    [System.NonSerialized]
    public Vector3 EndPosition;

    GameObject plane;

    public void Start() {
        plane = transform.GetChild(0).gameObject;
    }

    public void InithalizeMoveParameter(Vector3 startPosition, Vector3 endPosition, float moveSpeed = 10.0f) {
        this.StartPosition = startPosition;
        this.EndPosition = endPosition;
        this.moveSpeed = moveSpeed;
    }

    public void MoveMeshCutter() {
        transform.Translate(0.0f, -moveSpeed * Time.deltaTime, 0.0f);


        if (transform.position.y <= EndPosition.y) {
            transform.position = StartPosition;
        }
    }

    public void MeshCut() {

        plane.SetActive(false);

        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit)) {

            GameObject victim = hit.collider.transform.root.GetChild(0).gameObject;

            GameObject[] pieces = BLINDED_AM_ME.MeshCut.Cut(victim, transform.position, transform.up * -1, capMaterial);

            if (!pieces[1].GetComponent<Rigidbody>())
                pieces[1].AddComponent<Rigidbody>();

            Destroy(pieces[1], 1);
        }

    }

	void OnDrawGizmosSelected() {

		Gizmos.color = Color.green;

		Gizmos.DrawLine(transform.position, transform.position + transform.forward * 5.0f);
		Gizmos.DrawLine(transform.position + transform.right * 0.5f, transform.position + transform.right * 0.5f + transform.forward * 5.0f);
		Gizmos.DrawLine(transform.position + -transform.right * 0.5f, transform.position + -transform.right * 0.5f + transform.forward * 5.0f);

		Gizmos.DrawLine(transform.position, transform.position + transform.right * 0.5f);
		Gizmos.DrawLine(transform.position,  transform.position + -transform.right * 0.5f);

	}

    float moveSpeed;

}
